-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 10, 2018 at 10:30 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zadatak1`
--

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
CREATE TABLE IF NOT EXISTS `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv_filma` varchar(50) NOT NULL,
  `zanr_id` int(11) NOT NULL,
  `duzina` varchar(15) NOT NULL,
  `opis` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zanr_id` (`zanr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `naziv_filma`, `zanr_id`, `duzina`, `opis`) VALUES
(1, 'Gringo', 1, '111 min', 'Kratak sadržaj: Uz Šarliz Teron, Amandu Sejfrid, Džoela Edgertona i Dejvida Ojelovo, u filmu će svoj glumački debi ostvariti i ćerka preminulog kralja popa, Pariz Džekson.'),
(2, 'Gaša šeprtlja', 2, '85 min', 'Kratak sadržaj: Gaston je primljen kao pripravnik u uspešnoj kompaniji čiji je moto „učinimo beskorisno korisnim\". Gašu, kralja šeprtlji, zdrav razum zaobilazi u širokom luku, i zahvaljujući svojoj skoro natprirodnoj moći stalno izazova katastrofe što njegovog šefa dovodi do ludila!\r\n\r\nGlumci: Pierre-François Martin-Laval'),
(3, 'Izdaja', 5, '140 min', 'Kratak sadržaj: Priča je adaptacija špijunskog romana Džejsona Metjusa, smeštena u modernoj Rusiji, koja prati operativca Dominiku Jegorovu (Dženifer Lorens).\r\n\r\nGlumci: Jennifer Lawrence, Joel Edgerton, Matthias Schoenaerts, Mary-Louis Parker'),
(4, 'Ready player one', 4, '140 min', 'Kratak sadržaj: Nova avantura legendarnog reditelja i višestrukog oskarovca Stiven Spilberg, baziran na istoimenom bestseleru Ernesta Klinea.\r\n\r\nGlumci: Tye Sheridan, Olivia Cooke, Ben Mendelsohn, T.J. Miller, Simon Pegg, Mark Rylance, Hannah John-Kamen'),
(5, 'Nevesta', 3, '93 min', 'Kratak sadržaj: U davnoj prošlosti, jedan fotograf je održavao tradiciju fotografisanja umrlih članova svoje porodice. Kada mu je žena umrla, sahranio je pored tela device kako bi oživeo duh svoje žene u telu mlade devojke.\r\n\r\nGlumci: Victoria Agalakova, Vyacheslav Chepurchenko, Aleksandra Rebenok, Igor Khripunov, Natalia Grinshpun');

-- --------------------------------------------------------

--
-- Table structure for table `projekcija`
--

DROP TABLE IF EXISTS `projekcija`;
CREATE TABLE IF NOT EXISTS `projekcija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `film_id` int(11) NOT NULL,
  `vreme` varchar(10) NOT NULL,
  `sala` varchar(50) NOT NULL,
  `cena` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `film_id` (`film_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projekcija`
--

INSERT INTO `projekcija` (`id`, `film_id`, `vreme`, `sala`, `cena`) VALUES
(1, 2, '15:00', 'Merlinka', 350),
(3, 1, '15:00', 'Odrika', 400),
(4, 3, '17:00', 'Merlinka', 400),
(5, 4, '17:00', 'Odrika', 400),
(6, 1, '20:00', 'Merlinka', 400),
(7, 3, '20:00', 'Odrika', 450),
(8, 5, '22:30', 'Merlinka', 350),
(9, 4, '22:00', 'Odrika', 350);

-- --------------------------------------------------------

--
-- Table structure for table `zanr`
--

DROP TABLE IF EXISTS `zanr`;
CREATE TABLE IF NOT EXISTS `zanr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv_zanra` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zanr`
--

INSERT INTO `zanr` (`id`, `naziv_zanra`) VALUES
(1, 'akcioni'),
(2, 'komedija'),
(3, 'horor'),
(4, 'naučna-fantastika'),
(5, 'triler');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `film_ibfk_1` FOREIGN KEY (`zanr_id`) REFERENCES `zanr` (`id`);

--
-- Constraints for table `projekcija`
--
ALTER TABLE `projekcija`
  ADD CONSTRAINT `projekcija_ibfk_1` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
