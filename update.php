<?php

    define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
    require('inc/db_config.php');

    if(isset($_GET["id"]))
    $id = $_GET["id"];

    $sql = "SELECT film.id as 'id', film.naziv_filma as 'title', zanr.naziv_zanra as 'genre', film.duzina as 'duration', film.opis as 'description' FROM film
            INNER JOIN zanr ON film.zanr_id = zanr.id
            WHERE film.id=$id";
    $result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

    if(mysqli_num_rows($result)>0)
    {

        //$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
        //var_dump($row);
        
        while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) // MYSQLI_ASSOC, MYSQLI_BOTH,MYSQLI_NUM
        {
            $id = $row['id'];
            $title = $row['title'];
            $genre_id = $row['genre'];
            $duration = $row['duration'];
            $description = $row['description'];

        }

        mysqli_free_result($result);
    }

    mysqli_close($connection);

?>

<h2>Izmenite podatke<?php echo $id; ?></h2>

<form action="realUpdate.php" method="post">
    <lable>Naslov filma</lable><br>
    <input type="text" name="title" value="<?php echo $title; ?>"><br><br>
    <lable>Žanr(nepromenljivo, osim ako ne postanete reditelj)</lable><br>
    <input type="text" name="genre" value="<?php echo $genre_id; ?>" readonly><br><br>
    <lable>Vreme trajanja</lable><br>
    <input type="text" name="duration" value="<?php echo $duration; ?>"><br><br>
    <lable>Kratak opis</lable><br>
    <textarea name="description" rows="10" cols="50"><?php echo $description; ?></textarea><br><br>
    <input type="hidden" name="id" value="<?php echo $id; ?>"><br>
    <input type="submit" value="Update">
</form>