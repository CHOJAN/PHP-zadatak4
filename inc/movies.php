<main>
<?php
echo '<h2>Filmovi</h2>';

$sql = "SELECT film.id, film.naziv_filma, zanr.naziv_zanra, film.duzina, film.opis FROM film
        INNER JOIN zanr ON film.zanr_id = zanr.id";
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

if(mysqli_num_rows($result)>0)
{

    //$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
    //var_dump($row);
    echo "<table>
            <tr>
                <th>Br</th>
                <th>Naslov filma</th>
                <th>Žanr</th>
                <th>Dužina trajanja</th>
                <th>Kratak opis</th>
                <th>Izmeni podatke</th>
            </tr>";
    
    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) // MYSQLI_ASSOC, MYSQLI_BOTH,MYSQLI_NUM
    {
        $id = $row['id'];
        echo "<tr>
                <td>".$row['id']."</td>
                <td>".$row['naziv_filma']."</td>
                <td>".$row['naziv_zanra']."</td>
                <td>".$row['duzina']."</td>
                <td>".$row['opis']."</td>
                <td><a href=\"update.php?id=$id\">edit</a></td>
              </tr>";

    }
    echo "</table><br>";

    if ($result = mysqli_query($connection, "SELECT * FROM film")) {
        printf("Na listi je %d filmova.<br><br>\n", mysqli_num_rows($result));
    }

    mysqli_free_result($result);
}
?>
</main>