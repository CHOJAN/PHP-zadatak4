<main>
<?php

echo '<h2>Raspored projekcija</h2>';
$sql = "SELECT projekcija.id, film.naziv_filma, projekcija.vreme, projekcija.sala, projekcija.cena FROM projekcija
        INNER JOIN film ON projekcija.film_id = film.id";
//$sql = "SELECT * FROM projekcija";
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

if(mysqli_num_rows($result)>0)
{

    //$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
    //var_dump($row);
    echo "<table>
            <tr>
                <th>Br</th>
                <th>Naziv filma</th>
                <th>Vreme početka</th>
                <th>Sala</th>
                <th>Cena</th>
            </tr>";
    
    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) // MYSQLI_ASSOC, MYSQLI_BOTH,MYSQLI_NUM
    {
        $id=$row['id'];
        echo "<tr>
                <td>".$row["id"]."</td>
                <td>".$row["naziv_filma"]."</td>
                <td>".$row["vreme"]."</td>
                <td>".$row["sala"]."</td>
                <td>".$row["cena"]."</td>
              </tr>";

    }
    echo "</table><br>";

    if ($result = mysqli_query($connection, "SELECT * FROM projekcija")) {
        printf("Broj današnjih projekcija je %d.<br><br>\n", mysqli_num_rows($result));
    }

    mysqli_free_result($result);
}

mysqli_close($connection);
?>
</main>