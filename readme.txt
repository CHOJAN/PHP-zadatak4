Kreirati bazu podataka sa tri tabele: film, genre, schedule. Tabele treba povezati i uneti 5 podataka
u svaku od njih. Napraviti backup baze podataka preko fajla. Kreirati web stranicu index.php koja prikazuje
sve podatke iz sve tri tabele preko linkova (podaci da budu prikazani u tabeli). Kreirati web stranicu 
update.php koja na osnovu odabira podatka iz tabele film prikazuje podatke odabranog filma u obrascu.
Podaci sa obrasca se šalju na stranicu realUpdate.php koja menja podatke za odabrani film.