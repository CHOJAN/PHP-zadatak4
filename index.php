<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Movies</title>
        <meta name="description" content="Movies in MySQL database">
        <meta name="keywords" content="movies,genre,shedule">
        <meta name="author" content="Miroslav Cojanovic">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <link rel="stylesheet" type="text/css" href="css/normalize.css">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:600,700&amp;subset=latin-ext" rel="stylesheet">    <body>
        <div class="content">
        <?php
        define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!"); 
        require("inc/db_config.php");

        include("inc/header.html"); 
        include("inc/aside.html");
        
        if(!isset($_GET["link"])) 
        $_GET["link"]="index"; 
        
        switch ($_GET["link"]) {   
            
            case "movies":  
            include("inc/movies.php");  
            break;  
            
            case "genre":  
            include("inc/genre.php");  
            break;  
            
            case "schedule":  
            include("inc/schedule.php");  
            break; 
            
            default:  
            include("inc/movies.php");  
            break; 

            } 
        include("inc/footer.html"); ?>
        </div>
    </body>
</html>